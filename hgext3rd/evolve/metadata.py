# define Mercurial extension metadata for evolution
#
# Copyright 2017 Pierre-Yves David <pierre-yves.david@ens-lyon.org>
#
# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.

__version__ = '8.0.1'
testedwith = '4.3.2 4.4.2 4.5.2 4.6.1'
minimumhgversion = '4.3'
buglink = 'https://bz.mercurial-scm.org/'
