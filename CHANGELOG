Changelog
=========

8.0.1 -- 2018-06-11
-------------------

  * compatibility with mercurial 4.6.1
  * next-prev: respect commands.update.check config option (issue5808)
  * next-prev: fix `evolve --abort` on conflicts (issue5897)
  * obslog: fix breakage when commit has no description
  * amend: use context manager for locks (issue5887)
  * evolve: fix detection of interactive shell

topic (0.9.1)

  * topic: fix documentation formatting

8.0.0 -- 2018-04-25
-------------------

  * evolve: a new `--abort` flag which aborts an interrupted evolve
            resolving orphans,
  * `hg evolve` now return 0 if there is nothing to evolve,
  * amend: a new `--patch` flag to make changes to wdir parent by editing patch,
  * evolve: fixed some memory leak issue,
  * evolve: prevent some crash with merge and split (issue5833 and issue5832),
  * evolve: improvemed support for solving phase-divergence situation,
  * evolve: improvemed support for solving orphan situation,
  * obsdiscovery: added unit to various progress bar,
  * evolve: record "operation" for command where it was missing,

  * compatibility with Mercurial 4.6
  * drop support for Mercurial 4.1 and 4.2
  * `--obsolete` and `--old-obsolete` flags for `hg graft` are dropped
  * templatekw: remove `obsfatedata` templatekw. Individuals fields are
    available in core as single template functions.
  * topic: restraining name to letter, '-', '_' and '.'

7.3.0 -- 2018-03-21
---------------------

  * grab: new command to grab a changeset, put in on wdir parent
          and update to it
  * resolve: shows how to continue evolve after resolving all conflicts
  * evolve: `--continue` flag now continue evolving all the remaining revisions
  * prev and next now prompts user to choose a changeset in case of ambiguity
  * evolve: a new `--stop` flag which can be used to stop interrupted evolution

  * fold: fix issue related to bookmarks movement (issue5772)
  * amend: take lock before parsing the commit description (issue5266)
  * legacy: respect 'server.bundle1' config if any is set
  * previous: fix behavior on obsolete rev when topic is involved (issue5708)

7.2.1 --2018-01-20
-------------------

  * compatibility with future Mercurial 4.5
  * fix a packaging issue affect `hg evolve --continue`
  * fix "automatic" cache warming mode for push (client side)

7.2.0 -- 2018-01-15
-------------------

  * evolve: changes to the on disk format for interrupted evolve
  * evolve: --continue now propertly preserve phase information (issue5720)
  * evolve: --continue now properly reports merges as evolve
  * commit: suggest using topic on new heads
  * uncommit: `--revert` flag added to clean the wdir after uncommit
  * obslog: add color support to content-diff output with --patch
  * fix `hg prev` behavior on obsolete changesets
  * no longer issues "obsolete working copy" message during no-op

  * use the new instabilities names from mercurial 4.4+
    (in `hg evolve --list` and other messages)

  * new algorithm for obshashrange discovery:

    The new algorithm is faster, simpler to cache and with better complexity. It
    is able to handle repository of any size (naive python implementation is a
    bit slow). Support for the previous experimental approach have been
    dropped, please update both clients and servers. The new approach is still
    hidden behind and experimental flag for now.

topic (0.7.0)

  * fix compatibility with Mercurial-4.3
  * new template keyword `topic` to get changesets topic

7.1.0 -- 2017-12-12
-------------------

  * verbosity: respect --quiet for prev, next and summary
  * note: add a `-n/--note` flag to all history rewritting commands
  * obslog: shows the obsmarkers notes
  * obsdiscover: Improved stable range slice for the experimental obshashrange
                 (client and server need to upgrade to this version)
  * split: preserve the branch of the source changeset

topic (0.6.0)

  * add a new 'serverminitopic' extension for minimal server support
    (see `hg help -e serverminitopic` for details)
  * add a new config option `experimental.topic-mode.server` using which a
    server can reject draft changesets without topic
  * fix behavior of `hg stack` in cases of split
  * makes code more resilient to partiel initialization
  * avoid over wrapping inside of long living process

7.0.1 -- 2017-11-14
-------------------

  * obsdiscovery: allow the config option to disable discovery server side
    (it was previously only honored on the client side)

  * server: avoid exposing 'abort' to evolution enabled client talking
            to server with the extension bu obsolescence marker exchange
            disabled.

topic (0.5.1)

  * fix new-heads check when pushing new topic with --publish.

7.0.0 -- 2017-11-02
-------------------

  * drop compatibility with Mercurial 3.8, 3.9 and 4.0,
  * drop support for old and deprecated method to exchange obsmarkers,
  * forbid usage of the old pushbey based protocol to exchange obsmarkers,
  * evolve: rename '--contentdivergent' flag to '--content-divergent',
  * evolve: rename '--phasedivergent' flag to '--phase-divergent'.

topic (0.5.0)

  * add an experimental flag to enforce one head per name policy,
    (off by default, see 'hg help -e topic' for details)
  * add an experimental flag to have changesets without topic published on push,
    (off by default, see 'hg help -e topic' for details)
  * add a '--publish' flag to `hg push` (4.4+ only).

6.8.0 -- 2017-10-23
-------------------

  * compatibility with Mercurial 4.4
    (use upstream implementation for obsfate and effect-flags starting hg 4.4+)
  * pager: pager support to `obslog` and `evolve --list`

topic(0.4.0)

  * topic: fix handling of bookmarks and phases while changing topics.
           (mercurial 4.2 and above only)
  * topic: fix 'topic-mode' behavior when amending
  * pager: pager support to `topics` and `stack`

6.7.1 -- 2017-10-10
-------------------

  * obsfate: fix case were current user would disapear from the user list

topic (0.3.1)

  * topic: introduce a documented 'experimental.topic-mode' config
  * topic: add support for 'random' topic mode (see documentation for details)
  * stack: fix evolution preview for simple split.
  * fix a performance regression affecting all transactions.
    (the more non public changeset (hidden included), the slower)

6.7.0 -- 2017-09-27
-------------------

  * compatibility with change in future 4.4 at this release date,
  * documentation: improvement to content, wording and graphs,
  * obslog: improved templatability,
  * obslog/log: improve verb used to describe and evolution,
  * pstatus/pdiff: update to full command. They now appears in the help,
  * uncommit: add a --interactive option (4.3+ only).

topic (0.3.0)

  * push: add a --topic option to mirror --bookmark and --branch,
  * stack: improve display of interleaved topic,
  * stack: improve display of merge commit,
  * topic: add a new 'debugconvertbookmark' commands (4.3+ only),
    It helps migrating from bookmark feature branch to topic feature branch,
  * topic: --age flag also shows the user who last touched the topic,
  * topic: be more informative about topic activation and deactivation,
  * topic: gain a --current flag,
  * topic: small clarification and cleanup on various output.

6.6.0 -- 2017-07-25
-------------------

  - amend: add a --extract flag to move change back to the working copy,
    (same as uncommit, but accessible through the amend commit)
  - split: now properly refuse to split public changeset,
  - commands: unify and improve the pre-rewrite validation and error message,
  - uncommit: add support for --current-date and --current-user option,
  - fold: add support for --current-date and --current-user option,
  - metaedit: add support for --current-date and --current-user option,
  - split: add support for --current-date and --current-user option,
  - compat: use various new API instead of the one deprecated in 4.3,
    (when available)
  - documentation: various minor documentation update.

topic (0.2.0):

  - topic: add --age option to sort topic by the most recently touched,
  - topic: add a 't0' to access the root of a topic while keeping it active,
  - topic: allow 'hg prev' to me move to 't0',
  - topic: add a config option to enforce topic on new commit,
    (experimental.enforce-topic)
  - topic: make command names valid as expected, even if ui.strict=true.

6.5.0 -- 2017-07-02
-------------------

features:

 - obslog: gain a --patch flag to display changes introduced by the evolution
  (Currently limited to in simple case only)
 - log: display obsolescence fate by default, (future 4.3 only)
 - doc: various minor improvement.

bugfixes:

 - evolve: fix branch preservation for merge,
 - obsfate: improve support for advanced template reformating,
 - split: preserve author of the splitted changeset.
 - grab: properly fix hg executable on windows.

topic (0.1.0):

 - stack: also show the unstable status for the current changeset, (issue5553)
 - stack: properly abort when and unknown topic is requested,
 - stack: add basic and raw support for named branches,
 - topic: changing topic on revs no longer adds extra instability, (issue5441)
 - topic: topics: rename '--change' flag to '--rev' flag,
 - topic: multiple large performance improvements,
 - topic: various small output improvement,
 - topic: improved topic preservation for various commands.


6.4.0 -- 2017-06-16
-------------------

 - template: signifiant improvement to the '{obsfate}' template (now 4.2+ only)
 - template: fix 'successors' and 'precursors' template to expose hex-node
 - effect flag: the experiment is now active by default,
   (see 'hg help -e evolve' to opt out)
 - effect flag: fix a small bug related to hidden changeset,
 - obscache: reduce impact on large repository
 - obshashrange: install a '.max-revs' option see extension help for details

6.3.1 -- 2017-06-01
-------------------

 - also backport the "revelant-markers" fix when using "evolve.serveronly"

6.3.0 -- 2017-05-31
-------------------

 - olog: add an 'obslog' alias
 - olog: add an '--all' option to show the whole obsolescence history tree.
 - evolution: add an experiment to track the effect of rewrites.
   (See hg help - evolve for details)
 - exchange: fix the "relevant-markers" algorithm to include inline prune.
   This will impact discovery of obsmarkers between server and client if one
   still uses the old algorithm. Please upgrade both clients and servers as
   soon as possible.
   (See changeset 176d1a0ce385 in core Mercurial for details)
 - obsdiscovery: add a config flag to disable all obsmarkers discovery
   (See hg help - evolve for details)
 - template: add a 'precursors' template that display the closests precursors of changesets
 - template: add a 'successors' template that display the closests successors of changesets
 - template: add a 'obsfate' template that display how a changeset has evolved
 - new discovery experiment: add options to restrict memory consumption on
   large repository (see "hg help -e evolve" for details).
 - evolve: fix --rev handling in --list mode

6.2.1 -- 2017-05-23
-------------------

 - prune: fix a crash related to color handling,
 - next: fix a crash related to color handling,
 - discovery: document the 'obshashrange' experiment,
 - cache: reduce the warming load in case of reset,
 - cache: add a 'experimental.obshashcache.warm-cache' option to allow
   disabling post transaction cache warming.

6.2.0 -- 2017-05-18
-------------------

 - olog: a new command to inspect the obs-history of a changeset (hg-4.0 + only),
 - topic: have thg display topic name if possible,
 - blackbox: log more information about discovery and cache computation,
 - obscache: more efficient update in the (rare) case of a transaction adding
   markers without changesets,
 - obscache: fix more cache invalidation propagation,
 - obscache: also enable the new cache (from 6.1.0) for 'evolve.server-only',
 - obshashrange-cache: update incrementally in the (common) case of a
   transaction not affecting existing range,
 - obshashrange-cache: keep the cache warm after each transaction,
 - topic: now requires Mercurial 4.0 or above,
 - stack: now display if current revision is in bad state (issue5533),
 - stack: fix json output to be valid json.

6.1.0 -- 2017-05-03
-------------------

 - improve message about obsolete working copy parent,
 - improve message issued  when accessing hidden nodes (4.2 only),
 - introduce a new caches to reduce the impact of evolution on read-only commands,
 - add a 'experimental.auto-publish' config. See `hg help -e evolve` for details.
 - fix the propagation of some some cache invalidation,

6.0.1 -- 2017-04-20
-------------------

 - template: adapt to change in 4.2,
 - fix 'debugrecordpruneparents' (outdated API usage)
 - checkheads: give priority to updated 4.2 code,
 - serveronly: fix repository initialization.

6.0.0 -- 2017-03-31
-------------------

- push: improved detection of obsoleted remote branch (issue4354),
- drop compatibility for Mercurial < 3.8,
- removed old (unpackaged) pushexperiment extension,
- move all extensions in the official 'hgext3rd' namespace package,
- add the "topic" experimental extensions. See the README.topic file for details
- officially ship 'evolve.serveronly' extensions. That extensions contains
  only the part related to exchange and is intended to be used by server.

  Using the extension will enable evolution, use 'experimental.evolution=!'
  to disable obsmarkers echange.  The old '__temporary__.advertiseobsolete'
  option is no longer supported.

- a new prototype of obsmarker discovery is available. The prototype is still
  at early stage and not recommended for production.
  Examples of current limitations:

  - write access to the repo is highly recommanded for all operation,
  - large memory footprint,
  - initial caching is slow,
  - unusable on large repo (because of various issue pointed earlier),
  - likely to constains various bugs.

  It can be tested by setting `experimental.obshashrange=1` on both client and
  server. It is recommanded to get in touch with the evolve maintainer if you
  decide to test it.

- the 'debugrecordpruneparents' have been moved into the 'evolve.legacy'
  separate extension. enable that extentions if you need to convert/update
  markers in an old repository.

5.6.1 -- 2017-02-28
-------------------

- fix a crash that sometime happened when evolving merges.

5.6.0 -- 2017-02-01
-------------------

- compatibility with Mercurial 4.1.
- improvement of prune error message.
- fold: require --from flag for folding revisions to working copy
- fix crash when trying to fold an empty revision set (issue5453)
- uncommit: preserve copy information of remaining files (issue5403)

5.5.0 -- 2016-10-30
-------------------

- The {obsolete} template now yield "obsolete" or "".
- compatibility with Mercurial 4.0
- Fix erroneous manifest computation when solving 'bumped' changeset.
- split: avoid crash on empty commit (issue5191),
- next: improve locking to avoid issue with working copy parent (issue5244)
- prev: improve locking to avoid issue with working copy parent (issue5244)
- evolve: fix abort suggestion to include '.' in 'hg update -C .'

5.4.1 -- 2016-08-01
-------------------

 - compat with Mercurial 3.9

5.4.0 -- 2016-05-06
-------------------

- Some collaboration with the topic experimental extensions,
  - hg evolve --all with consider all troubles in your current topic,
  - preserve 'topic' during evolve,
  - 'next' and 'prev' restrict themself to the current topic by default,
- remove the dangerous 'kill' alias for 'prune' (because 'hg kill -1' without
  the leading 'hg' will give you an hardtime)
- during 'hg evolve' skip unsupported merge instead of aborting
- various documentation fix and update
- hg summary now suggest 'hg evolve --continue when appropriate`
- compatibility with Mercurial 3.8 'hgext' namespace package.
- small improvement to the `hg split` instruction
- add a 'metaedit' command to rewrite changeset meta data.

5.3.0 -- 2016-02-11
-------------------

- split: add a new command to split changesets,
- tests: drop our copy of 'run-tests.py' use core one instead,
- bookmark: do all bookmark movement within a transaction.
- evolve: compatibility with Mercurial 3.7
- evolve: support merge with a single obsolete parent (hg-3.7+ only)
- evolve: prevent added file to be marked as unknown if evolve fails (issue4966)
- evolve: stop relying on graftstate file for save evolve state
          (for `hg evolve --continue`)
- evolve: fix divergence resolution when it result in an empty commit
          (issue4950) (hg-3.5+ only)
- no longer lock the repository for `hg parents` (issue4895)
- updated help for the `evolve` command

5.2.1 -- 2015-11-02
-------------------

- add compatibility with Mercurial 3.6
- prune: fixed possible issue with lock and bookmark
- next/prev: fixed possible issue with lock and bookmark
- add some progress data during changesets discovery
- take advantage of dirstate/transaction collaboration

5.2.0 -- 2015-06-25
-------------------

- evolve: gain a --rev option to control what revisions to evolve (issue4391)
- evolve: revision are processed in the order they stack on destination
- evolve: properly skip unstable revision with non-evolved unstable parent
- evolve: gain --unstable --divergent --bumped flag to select the trouble
- evolve: issue more useful error message and hint when evolve has nothing to
          do as invocated.
- evolve: bare `hg evolve` commands now abort when multiple changesets could be
          a target.
- evolve: `hg evolve --all` only evolve changeset that will end up as
          descendant of the current working copy. The old behavior of `--all`
          in now in `--all --any`.
- evolve: add a 'experimental.evolutioncommands' for fine grained commands
          enabling
- next/prev: requires `--merge` to move with uncommitted changes
- next: significantly reword error messages
- next: add a --evolve flag to evolve aspiring children when on a head

5.1.5 -- 2015-06-23
-------------------

- minor documentation cleanup
- support -i option for `hg amend` if commit supports it (3.4)
- fix the `debugrecordpruneparents` utility
- fix some possible crash during command abort (release nonexistent transaction)
- fix simple4server bug tracker URL
- compatibility with bookmark API change in future Mercurial 3.5
- prune no longer move the active bookmark for no reason (issue4559)
- evolve: stop reporting divergence base as missing when we actually have it
- significant performance improvement for all revsets.
- provide a hint of how to update to the successor of an obsolete working copy
  parent.

5.1.4 -- 2015-04-23
-------------------

- significant documentation update
- fix issue4616: pulling with bundle2 would crash if common marker when
  discovered on non-served changesets.
- fix the debugobsrelsethashtree command

5.1.3 -- 2015-04-20
-------------------

- discovery: fix misbehaving discovery across python version
- pull: properly install the bundle2 par generator
  (avoid sending all markers for each pull)
- commit: avoid potential deadlock (acquires wlock before lock)
- graft: avoid potential deadlock (acquires wlock before lock)

5.1.2 -- 2015-04-01
-------------------

- evolve: prevent a crash in httpclient_pushobsmarkers() when pushing

5.1.1 -- 2015-03-05
-------------------

- debugobsconvert: fix invalid markers during conversion
- discovery: cache some of the obs hash computation to improve performance (issue4518)
- revset: fix some crash with (issue4515)

5.1 -- 2015-01-30
-------------------

- evolve: explicitly disable bookmark on evolve (issue4432)
- evolve: don't abort Mercurial on version mismatch
- compatibility with mercurial 3.3

5.0.2 -- 2014-12-14
-------------------

- evolve: remove dependency to the rebase extension

5.0.1 -- 2014-11-25
-------------------

- amend: fix --logfile argument
- evolve: preserve branch change when evolving
- evolve: fix potential crash while solving `bumped` changesets.
- uncommit: abort when rev specifies the current changeset
- evolve: various message improvement
- evolve: fix selection of changeset to evolve from the middle of a stack (issue4434)
- evolve: make next/prev only move bookmarks optionally
- evolve: tell user which "base of divergent changeset" is not found

5.0.0 -- 2014-10-22
-------------------

- drop compat with Mercurial pre 3.2
- uncommit: add a --rev argument
- evolve: add a `working directory now at xxxxxxxxxx` message
- evolve: automatically translate obsolete hashes when evolving
- properly skip marker creating if patch apply cleanly
- prune: work around a massive slowdown from lazy revset
- grab: "fix" the grab alias on window

- fix an issue where prune performance were quadratic with the number of
  changesets pruned.
- pull: use discovery to pull less obsmarkers through bundle2


4.1.0 -- 2014-08-08
-------------------

- amend: add -D/--current-date option
- amend: add -U/--current-user option
- evolve: add a --tool option
- evolve: add a --confirm option
- mark "commit -o", "graft -o" and "graft -O" as deprecated since they are
  unlikely to eventually make it into core.
- push obsmarkers and phases in the same transaction than changesets
  (when using hg >= 3.1 and bundle2-exp is enabled)
- hide message about the obsolescence marker exchange behind a
  `experimental.verbose-obsolescence-exchange` variable (default to False).

4.0.1 -- 2014-08-08
-------------------

- createmarkers() accept an iterable (for compat with other extension)

4.0.0 -- 2014-06-03
-------------------

- require Mercurial version 3.0.1 or above
- some compatibility fixes with future 3.1.0
- deprecated `gup` and `gdown` in favor of prev and next
- record parent of pruned parent at prune time
- added a `debugobsstorestat` command to gather data on obsmarker content.
- added a `debugrecordpruneparents` command to upgrade existing prune marker
  with parent information. Please run it once per repo after upgrading.
- improvement to obsolescence marker exchange:
  - added progress when pushing obsmarkers
  - added multiple output during obsolescence markers exchange
  - only push markers relevant to pushed subset
  - add a new experimental way to exchange marker (when server support):

    - added progress when pulling obsmarkers
    - only pull markers relevant to pulled subset
    - avoid exchanging common markers in some case
    - use bundle2 as transport when available.

 - add a hook related to the new commands

3.3.2 -- 2014-05-14
-------------------

- fix a bug where evolve were creating changeset with 2 parents on windows
  (fix issues #16, #35 and #42)
- adds a --obsolete flag to import (requires Mercurial 3.0)
- prune: update to successor rather than parent when pruning '.' with -s
- fold: add missing --message and --logfile option
- fold: add squash as an alias

3.3.1 -- 2014-04-23
-------------------

- various language fix
- active bookmark now move when using prev/next (#37)
- fix some preservation of rename information on evolve (#33)
- abort when evolve tries to move a node on top of itself (will helps on the #35 front)
- fold: enable --date and --user options

3.3.0 -- 2014-03-04
-------------------

- raise Mercurial's minimal requirement to 2.7
- drop `latercomer` and `conflicting` compatibility. Those old alias are
  deprecated for a long time now.
- add verbose hint about how to handle corner case by hand.
  This should help people until evolve is able to to it itself.
- removed the qsync extension. The only user I knew about (logilab) is not
  using it anymore. It not compatible with coming Mercurial version 2.9.
- add progress indicator for long evolve command
- report troubles creation from `hg import`

3.2.0 -- 2013-11-15
-------------------

- conform to the Mercurial custom of lowercase messages
- added a small extension to experiment with obsolescence marker push
- amend: drop the deprecated note option
- amend: use core mechanism for amend (fix multiple bugs)
- parents command: add "working directory parent is obsolete" message
- evolve command: allow updating to the successor if the parent is
  obsolete
- gdown and gup commands: add next and previous alias, respectively
- make grab aliases compatible with Mercurial 2.8
- Tested with 2.6, 2.7 and 2.8

3.1.0 -- 2013-02-11
-------------------

- amend: drop deprecated --change option for amend
- alias: add a grab alias to be used instead of graft -O
- touch: add a --duplicate option to *not* obsolete the old version
- touch: fix touching multiple revision at the same time
- evolve: add a --all option
- prune: various minor improvements
- prune: add option to prune a specific bookmark
- prune: add -u and -d option to control metadata

3.0.0 -- 2013-02-02
-------------------

- compatibility with 2.5

2.2.0 --
-------------------

- make evolve smarter at picking next troubled to solved without --any

2.1.0 -- 2012-12-03
-------------------

- qsync fixes
- have qfold ask for commit message

2.0.0 -- 2012-10-26
-------------------

- compat with mercurial 2.4

1.1.0 -- 2012-10-26
-------------------

- fix troubles creation reporting from rebase
- rename latecomer to bumped
- renamed conflicting to divergent
- smarter divergent handling

1.0.2 -- 2012-09-19
-------------------

- fix hg fold bug
- fix hg pull --rebase
- fix detection of conflict with external tools
- adapt to core movement (caches and --amend)

1.0.1 -- 2012-08-31
-------------------

- documentation improvement
- fix a performance bug with hgweb

1.0 -- 2012-08-29
-------------------

- Align with Mercurial version 2.3 (drop 2.2 support).
- stabilize handle killed parent
- stabilize handle late comer
- stabilize handle conflicting
- stabilize get a --continue switch
- merge and update ignore extinct changeset in most case.
- new "troubled()" revset
- summary now reports troubles changesets
- new touch command
- new fold command
- new basic olog alias

- rebase refuse to work on public changeset again
- rebase explicitly state that there is nothing to rebase because everything is
  extinct() when that happen.
- amend now cleanly abort when --change switch is misused


0.7 -- 2012-08-06
-------------------

- hook: work around insanely huge value in obsolete pushkey call
- pushkey: properly handle abort during obsolete markers push
- amend: wrap the whole process in a single transaction.
- evolve: tweak and add EOL to kill warning
- obsolete: fix doc, rebase no longer aborts with --keep
- obsolete/evolve: fix grammar in prerequisite messages
- evolve: avoid duplication in graft wrapper
- evolve: graft --continue is optional, test

0.6 -- 2012-07-31
-------------------

- obsolete: change warning output to match mercurial core on
- qsync: ignore nonexistent nodes
- make compat server both compatible with "dump" and "dump%i" version

0.5 -- 2012-07-16
-------------------

- obsolete: Detect conflicting changeset!
- obsolete: adapt to core: marker are written in transaction now
- evolve: add the solve alias to obsolete
- doc: big update of terms and summary of the concept
- evolve: switch the official name for "kill" to prune


0.4.1 -- 2012-07-10
-------------------

- [convert] properly exclude null successors from conversion
- Ignore buggy marker in newerversion


0.4.0 -- 2012-07-06
-------------------

- obsolete: public changeset are no longer latecomer.
- obsolete: move to official binary format
- adapt for new mercurial
- obsolete: we are not compatible with 2.1 any more

0.3.0 -- 2012-06-27
-------------------

- obsolete:  Add "latecomer" error detection (stabilize does not handle resolution yet)
- evolve:    Introduce a new `uncommit` command to remove change from a changeset
- rebase:    allow the use of --keep again
- commit:    --amend option create obsolete marker (but still strip)
- obsolete:  fewer marker are created when collapsing revision.
- revset:    add, successors(), allsuccessors(), precursors(), allprecursors(),
             latecomer() and hidden()
- evolve:    add `prune` alias to `kill`.
- stabilize: clearly state that stabilize does not handle conflict
- template:  add an {obsolete} keyword

0.2.0 -- 2012-06-20
-------------------

- stabilize: improve choice of the next changeset to stabilize
- stabilize: improve resolution of several corner case
- rebase:    handle removing empty changesets
- rebase:    handle --collapse
- evolve:   add `obsolete` alias to `kill`
- evolve:   add `evolve` alias to `stabilize`
